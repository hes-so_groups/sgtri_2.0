using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class ServeurConnexion : MonoBehaviour
{
    [System.Serializable]
    public class Fact
    {
        public string fact;
        public int length;
    }
    [SerializeField] private string urlConnexion = "https://nursing.p645.hevs.ch/";

    //Get Ref UI
   [SerializeField] private TextMeshProUGUI labelConnexion;
   [SerializeField] private TextMeshProUGUI labelResultConnexion;


    void Start()
    {
        // A correct website page.
        //StartCoroutine(GetRequest("https://jzjeff.pythonanywhere.com"));
        labelConnexion.text = "Connexion to :" + urlConnexion;
        StartCoroutine(GetRequest(urlConnexion));


        //StartCoroutine(GetText());
        ////Send
        //StartCoroutine(Upload());
    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    GetResulToUI(false, pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    GetResulToUI(false, pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    GetResulToUI(true, pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    break;
            }
        }
    }

    IEnumerator Upload()
    {
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection("field1=foo&field2=bar"));
        formData.Add(new MultipartFormFileSection("my file data", "myfile.txt"));

        UnityWebRequest www = UnityWebRequest.Post("https://jzjeff.pythonanywhere.com/myform", formData);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("Upload() " + www.error);
        }
        else
        {
            Debug.Log("Upload() " + "Form upload complete!");
        }
    }

    IEnumerator GetText()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://jzjeff.pythonanywhere.com/web/fact.json");
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("GEtTEXT() " + www.error);
        }
        else
        {
            // Show results as text
            Debug.Log("GEtTEXT() " + www.downloadHandler.text);

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;

            var test = www.downloadHandler.text;
            Fact testInt = JsonUtility.FromJson<Fact>(test);
            Debug.Log("GEtTEXT() " + "testInt :" + testInt.fact);
        }

        IEnumerator SendText()
        {
            UnityWebRequest www = UnityWebRequest.Get("https://jzjeff.pythonanywhere.com/web/fact.json");
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("GEtTEXT() " + www.error);
            }
            else
            {
                // Show results as text
                Debug.Log("GEtTEXT() " + www.downloadHandler.text);

                // Or retrieve results as binary data
                byte[] results = www.downloadHandler.data;

                var test = www.downloadHandler.text;
                Fact testInt = JsonUtility.FromJson<Fact>(test);
                Debug.Log("GEtTEXT() " + "testInt :" + testInt.fact);
            }
        }

    }

    private void GetResulToUI(bool isSucces, string resultInfo)
    {
        if (isSucces)
        {
            labelResultConnexion.GetComponent<TextMeshProUGUI>().color = Color.green;
        }
        else
        {
            labelResultConnexion.GetComponent<TextMeshProUGUI>().color = Color.red;
        }

        labelResultConnexion.text = resultInfo;
    }
}
